/*
   Android Instrument Function Debug

   This is the file which must be included into the program which should
   be debugged.

   Written by: Ivan 'w23' Avdeev, marflon@gmail.com, 2010

   Contributions:
     - 2011/01/15 - Sergey A. Galin added logging of run-time offset of __cyg_profile_func_enter().
*/
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <android/log.h>

const char* const kCallLog = "/sdcard/andrinst.log";
const char* const kOffsetFile = "/sdcard/andrinst.off";

extern "C"
{
    void __cyg_profile_func_enter (void *, void *) __attribute__((no_instrument_function));
    void __cyg_profile_func_exit (void *, void *) __attribute__((no_instrument_function));
}

static FILE *f = 0;

static struct 
{
    uint32_t idx; 
    uint32_t thread;
    void* caller;
    void* func;
} entry;

pthread_mutex_t dump_mutex = PTHREAD_MUTEX_INITIALIZER;

void __cyg_profile_func_enter (void *func, void *caller)
{
    pthread_mutex_lock(&dump_mutex);
    int errsv;
    if (f==0)
    {
        entry.idx = 0;
        f = fopen(kCallLog, "wb");
        if (f==0)
        {
            errsv = errno;
            __android_log_print(ANDROID_LOG_ERROR, "AndroidInstrumentDebug",
                 "Failed to create call log file %s: %d - "
                 "Please make sure you added write-external-storage permission to the manifest.",
                 kCallLog, int(errsv));
            exit(errsv);
        }

        FILE* offf = fopen(kOffsetFile, "w");
        if (offf==0)
        {
            errsv = errno;
            __android_log_print(ANDROID_LOG_ERROR, "AndroidInstrumentDebug", 
                "Failed to create offset file %s: %d", kOffsetFile, int(errsv));
            exit(errsv);
        }
        fprintf(offf, "%lx", (unsigned long)__cyg_profile_func_enter);
        fclose(offf);

        __android_log_print(ANDROID_LOG_DEBUG, "AndroidInstrumentDebug", "Will dump stack. Entry size %d", int(sizeof(entry)));
    }

    entry.thread = (uint32_t)gettid();
    entry.caller = caller;
    entry.func = func;
    errsv = fwrite(&entry, sizeof(entry), 1, f);
    ++entry.idx;
    if (errsv != 1)
        __android_log_print(ANDROID_LOG_ERROR, "AndroidInstrumentDebug", "FAILURE!");
    else
        fflush(f);
    //__android_log_print(ANDROID_LOG_DEBUG, "AndroidInstrumentDebug", "%04lu > %p %p\n", (long)gettid(), caller, func);
    pthread_mutex_unlock(&dump_mutex);
}

void __cyg_profile_func_exit (void *func, void *caller)
{
    //__android_log_print(ANDROID_LOG_DEBUG, "AndroidInstrumentDebug", "%04lu < %p %p\n", (long)gettid(), caller, func);
    pthread_mutex_lock(&dump_mutex);
    entry.idx |= 0x80000000;
    entry.thread = (uint32_t)gettid();
    entry.caller = caller;
    entry.func = func;
    fwrite(&entry, sizeof(entry), 1, f);
    entry.idx &= 0x7fffffff;
    ++entry.idx;
    fflush(f);
    pthread_mutex_unlock(&dump_mutex);
}

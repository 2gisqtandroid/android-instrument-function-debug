#!/bin/bash

# Android Instrument Function Debug
#
# Stack trace parser wrapper script.
#
# Written by: Sergey 'TL1' Galin, sergey.galin@gmail.com, 2011

# Uncomment the following line to keep temporary files:

#SCRIPTDEBUG=yes

ARMEABINM=`which arm-eabi-nm`
if [ ! -x "$ARMEABINM" ] ; then
  ARMEABINM="/usr/local/android-ndk-r5/toolchains/arm-linux-androideabi-4.4.3/prebuilt/linux-x86/bin/arm-linux-androideabi-nm" ;
fi
if [ ! -x "$ARMEABINM" ] ; then
  ARMEABINM="/usr/local/android-ndk-r4-crystax/build/prebuilt/linux-x86/arm-eabi-4.4.0/bin/arm-eabi-nm" ;
fi
if [ ! -x "$ARMEABINM" ] ; then
  ARMEABINM="/usr/local/android-ndk-r4/build/prebuilt/linux-x86/arm-eabi-4.4.0/bin/arm-eabi-nm" ;
fi
if [ ! -x "$ARMEABINM" ] ; then
  ARMEABINM="/usr/local/qadk-r4/build/prebuilt/linux-x86/arm-eabi-4.4.0/bin/arm-eabi-nm" ;
fi
if [ ! -x "$ARMEABINM" ] ; then
  echo "Could not find arm-eabi-nm executable!"
  exit 1 ;
fi
echo "Using nm: $ARMEABINM"

ADB=`which adb`
if [ ! -x "$ADB" ] ; then
  echo "adb (Android debugger) executable is not in PATH!"
  exit 1 ;
fi

SONAME="$1"
OUTFILE="stackrace.txt"

if [ "$SONAME" = "" ] ; then
  echo "USAGE: $0 <yourapplication.so>"
  echo "Example: $0 myapp.so"
  echo "Output stack trace will be written into \"$OUTFILE\"."
  exit 0 ;
fi
if [ ! -f "$SONAME" ] ; then
  echo "File not found: $SONAME"
  exit 0 ;
fi

echo "Binary file name: $SONAME"
echo "Output file: $OUTFILE"

if [ "$SCRIPTDEBUG" = "yes" ] ; then
  FUNCTIONLIST="functions.txt"
  LOCALLOG="andrinst.bin"
  LOCALOFF="andrinst.off"
  LOCALOFF2="andrinst-so.off"
  echo "Keeping intermediate files." ;
else
  FUNCTIONLIST=`tempfile`
  LOCALLOG=`tempfile -s .bin`
  LOCALOFF=`tempfile`
  LOCALOFF2=`tempfile`
  echo "Using temporary intermediate files." ;
fi


DEVLOG="/sdcard/andrinst.log"
DEVOFF="/sdcard/andrinst.off"

function cleanup () {
  if [ ! "$SCRIPTDEBUG" = "yes" ] ; then
    echo "Cleaning up..."
    rm -f "$LOCALLOG"
    rm -f "$LOCALOFF"
    rm -f "$LOCALOFF2"
    rm -f "$FUNCTIONLIST" ;
  else
    echo "Not cleaning up (debug mode)." ;
  fi
}

echo "Generating list of functions in the binary..."
"$ARMEABINM" "$SONAME" | grep ^[0-9a-f] | sort > "$FUNCTIONLIST" || exit 1

echo "Downloading call log..."
# Get the log from the device
adb pull "$DEVLOG" "$LOCALLOG" || exit 1
if [ ! -f "$LOCALLOG" ] ; then
  echo "Failed to pull stack dump from $DEVLOG to $LOCALLOG"
  cleanup
  exit 1 ;
fi
adb pull "$DEVOFF" "$LOCALOFF" || exit 1
if [ ! -f "$LOCALOFF" ] ; then
  echo "Failed to pull stack dump from $DEVOFF to $LOCALOFF"
  cleanup
  exit 1 ;
fi

function first () {
  echo $1
}

FUNCLINE=`cat "$FUNCTIONLIST" | grep __cyg_profile_func_enter`
first $FUNCLINE > "$LOCALOFF2"
SOOFFSET=`cat "$LOCALOFF2"`

if [ "$SOOFFSET" = "" ] ; then
  echo "Could not find .so offset of __cyg_profile_func_enter - did you supply an unstripped library?"
  cleanup
  exit 1 ;
fi

RUNTIMEOFFSET=`cat "$LOCALOFF"`
let LOADOFFSETDEC=0x$RUNTIMEOFFSET-0x$SOOFFSET
LOADOFFSET=`printf "%X" $LOADOFFSETDEC`

echo "Library offset: $SOOFFSET, run-time offset: $RUNTIMEOFFSET, load offset: $LOADOFFSET"

echo "Parsing call stack..."
# Parse the log and the function list and write stack trace
instrstackparser $LOADOFFSET "$FUNCTIONLIST" "$LOCALLOG" > "$OUTFILE"

cleanup

echo "Done!"


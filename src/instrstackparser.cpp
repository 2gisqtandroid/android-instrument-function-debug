/*
   Android Instrument Function Debug

   Stack dump parser.

   Written by: Ivan 'w23' Avdeev, marflon@gmail.com, 2010
*/

// compile:
//    g++ inststackparse2.cpp
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <map>
#include <stack>

struct stack_entry_t {
    int line;
    unsigned long caller, func;
};

typedef std::stack<stack_entry_t> call_stack_t;

struct thread_info_t {
    int id;
    call_stack_t stack;

    thread_info_t(int& _id)
        : id(_id) { ++_id; }
};

typedef std::map<unsigned long, char*> func_map_t;
typedef std::map<unsigned long, thread_info_t*> thread_map_t;

func_map_t functions;

void func_printf(FILE* f, unsigned long addr)
{
    func_map_t::iterator fm_i = functions.find(addr);
    if (fm_i != functions.end())
        fprintf(f, "%s", (*fm_i).second);
    else
        fprintf(f, "%08lx", addr);
}

void print_usage(char* name)
{
    fprintf(stderr, "usage: %s so_load_shift functions.list stack.trace > stack.named\n", name);
    fprintf(stderr, "\tso_load_shift is in hexadecimal format\n");
    fprintf(stderr, 
      "\tfunctions.list is prepared as follows:\n"
      "\tarm-eabi-nm lib.so | grep ^[0-9a-f] | sort > functions.list\n");
//    fprintf(stderr, "\tstack.trace is generated using -finstrument-functions and should have format:\n\t\t\thread_id_dec {>, <} caller_addr_hex function_addr_hex\n\t\t> -- enter, < -- exit\n");
}

int main(int argc, char* argv[])
{
    if (argc != 4)
    {
        print_usage(argv[0]);
        return EINVAL;
    }

    // read functions
    int errsv;
    FILE* f = fopen(argv[2], "r");
    if (f == 0)
    {
        errsv = errno;
        fprintf(stderr, "failed to open file \"%s\": %d\n", argv[2], errsv);
        return errsv;
    }

    // parse and fill
    int line = 0;
    while(!feof(f))
    {
        unsigned long addr;
        char* name = 0;
        errsv = fscanf(f, "%lx %*c %as", &addr, &name);
        if (errsv != 2)
            break;
        functions[addr] = name;
        ++line;
    }
    fclose(f);
    f = 0;
    fprintf(stderr,"read %d lines in file \"%s\"\n", line, argv[2]);

    // uh-oh
    unsigned long shift = strtoul(argv[1], 0, 16);
    fprintf(stderr, "using .so load shift %08lx\n", shift);

    // begin parsing 
    bool binary = (strstr(argv[3],".bin")!=0);
    f = fopen(argv[3], "r");
    if (f==0)
    {
        errsv = errno;
        fprintf(stderr, "failed to open file \"%s\": %d\n", argv[3], errsv);
        return errsv;
    }

    // parse!
    int depth = -1;
    stack_entry_t entry = {0, 0, 0};
    char mode;
    thread_map_t threads;
    thread_info_t* tinfo;
    unsigned long thread;
    int next_thread_id = 0;
    while (!feof(f))
    {
        if (binary)
        {
            static struct
            {
                uint32_t idx;
                uint32_t thread;
                uint32_t caller;
                uint32_t func;
            } bin_entry;
            errsv = fread(&bin_entry, sizeof bin_entry, 1, f);
            if (errsv != 1)
                break;
            mode = ((bin_entry.idx&0x80000000)==0)?'>':'<';
            entry.caller = bin_entry.caller;
            entry.func = bin_entry.func;
            thread = bin_entry.thread;
            if (entry.line != (bin_entry.idx&0x7fffffff))
                fprintf(stderr, "broken index @ entry %d != idx %d", entry.line, bin_entry.idx&0x7fffffff);
        } else {
            errsv = fscanf(f, "%lu %c %lx %lx", &thread, &mode, &entry.caller, &entry.func);
            if (errsv != 4)
                break;
        }

        thread_map_t::iterator t_i = threads.find(thread);
        if (t_i == threads.end())
        {
            fprintf(stderr, "new thread %d at line %d\n", next_thread_id, entry.line);
            fprintf(stdout, "NEW THREAD ID %d (real %08lx)\n", next_thread_id, thread);
            tinfo = new thread_info_t(next_thread_id);
            threads[thread] = tinfo;
        } else
            tinfo = (*t_i).second;

        fprintf(stdout, "%8d: %2d ", entry.line, tinfo->id);

        if (mode == '>' )
        {
            int ret = 0;
            if (tinfo->stack.size() > 0)
                ret = tinfo->stack.top().line;
            fprintf(stdout, "--> (%4d, ret %d) ", int(tinfo->stack.size()), int(ret));
            tinfo->stack.push(entry);
            func_printf(stdout, entry.func-shift);
            //func_printf(stdout, entry.caller-shift);
            fprintf(stdout, "\n");
        } else if (mode == '<') {
            fprintf(stdout, "<-- ");
            if (tinfo->stack.size() == 0) {
                fprintf(stdout, "BROKEN, WHERE TO RETURN???\n");
            } else {
                stack_entry_t& prev = tinfo->stack.top();
                if (prev.func != entry.func ||
                    prev.caller != entry.caller)
                    fprintf(stdout, "BROKEN ");
                fprintf(stdout, "ret to %d\n", prev.line);
                tinfo->stack.pop();
            }
        } else {
            fprintf(stderr, "mode %c is invalid\n", mode);
        }
        ++entry.line;
    }

    // dump the remaining stack
    fprintf(stdout, "\nREMAINDER:\n");
    thread_map_t::iterator t_i = threads.begin();
    while (t_i != threads.end())
    {
        tinfo = (*t_i).second;
        fprintf(stdout, "thread id %d (real %08lx): %d\n", int(tinfo->id), long((*t_i).first), int(tinfo->stack.size()));
        while (tinfo->stack.size() > 0)
        {
            stack_entry_t& prev = tinfo->stack.top();
            fprintf(stdout, "\t (%4d, ret %d) --- ", int(tinfo->stack.size()), int(prev.line));
            func_printf(stdout, prev.func-shift);
            fprintf(stdout, "\n");
            tinfo->stack.pop();
        }
        fprintf(stdout, "\n");
        ++t_i;
    }

    fclose(f);

    return 0;
}
